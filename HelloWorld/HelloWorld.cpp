#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"


using namespace llvm;

namespace {
struct HelloWorld : public FunctionPass {

	static char ID;
	HelloWorld() : FunctionPass(ID) {}

	bool runOnFunction(Function &F) override {
		errs() << "Hello: ";
		errs().write_escaped(F.getName()) << '\n'; 
		return false;
	}
};
}

char HelloWorld::ID = 0;
static RegisterPass<HelloWorld> X("HelloWorld", "Hello World Pass");
